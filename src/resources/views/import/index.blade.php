@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="list-group">
            <a href="#" class="list-group-item list-group-item-action active">
                Import
            </a>

            @if (Route::has('imports.pds.index'))
                <a href="{{route('imports.pds.index')}}" class="list-group-item list-group-item-action">Professional Development</a>
            @endif

            @if (Route::has('imports.uni.index'))
                <a href="{{route('imports.uni.index')}}" class="list-group-item list-group-item-action">University Organization</a>
            @endif

        </div>

    </div>
@endsection
