<div class="dropdown-menu" aria-labelledby="yeltrik-import">

    @includeIf('import::nav-bar.nav-item.dropdown.dropdown-menu.dropdown-item.index')
    <div class="dropdown-divider"></div>

    @includeIf('teachingHonors::nav-bar.nav-item.dropdown.dropdown-menu.dropdown-item.index')
    @includeIf('importPD::nav-bar.nav-item.dropdown.dropdown-menu.dropdown-item.index')
    @includeIf('importProfileAsana::nav-bar.nav-item.dropdown.dropdown-menu.dropdown-item.index')
    @includeIf('importProfileAsanaUniMbr::nav-bar.nav-item.dropdown.dropdown-menu.dropdown-item.index')
    @includeIf('importProfileAsanaUniOrg::nav-bar.nav-item.dropdown.dropdown-menu.dropdown-item.index')
    @includeIf('importUniAsana::nav-bar.nav-item.dropdown.dropdown-menu.dropdown-item.index')

</div>
